import Vue from 'vue';
import Vuex from 'vuex';
import modules from './modules';

// Load Vuex
Vue.use(Vuex);
//Create store
export default new Vuex.Store({
  modules,
  strict: process.env.NODE_ENV !== 'production',
});
